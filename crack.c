#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    char *guesshash = md5(guess, strlen(guess));
    
    // Compare the two hashes
    if (strcmp(hash, guesshash) == 0)
    {
        free(guesshash);                    // Free malloc'd memory
        return 1;                           /* Return 1 if true */
    }    
    else
    {
        free(guesshash);                    // Free malloc'd memory
        return 0;                          /* Return 0 if false */
    }    
}

// Read in the dictionary file and return the array of strings
// and store the length of the array in size.
// This function is responsible for opening the dictionary file,
// reading from it, building the data structure, and closing the
// file.
char **read_dictionary(char *filename, int *size)
{
    // Use stat to find out how big the file is
    struct stat info;
    if (stat(filename, &info) == -1){
        printf("Can't stat the file\n" );
        exit(1);
    }
    int filesize = info.st_size;
    //printf("File is %d bytes\n", filesize);
    
     // Allocate memory for the file, plus one for the null
    char *contents = malloc(filesize + 1);
    
    FILE *in = fopen(filename, "rb");
    if (!in)
    {
        printf("Can't open file for reading");
        exit(1);
    }
    fread(contents, 1, filesize, in);
    fclose(in);
    
     // Place null character on end to make it a string
    contents[filesize] = '\0';
    
    // Count the newlines
    int nlcount = 0;
    for (int i = 0; i < filesize; i++)
    {
        if (contents[i] == '\n') nlcount++;
    }
    
    printf("Dictionary line count is %d\n", nlcount);
    
    // Allocate memory for array of strings
    char **lines = malloc(nlcount * sizeof(char *));
    
    // Fill in array of string pointers
    lines[0] = strtok(contents, "\n");
    int i = 1;
    while ((lines[i] = strtok(NULL, "\n")) != NULL)
    {
        i++;
    }

    // Return values
    *size = nlcount;
    return lines;
    
    // Key variables
    // contents = pointer to the entire file
    // lines = pointer to an array of strings
    // nlcount = number of strings
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the dictionary file into an array of strings.
    int dlen;                       
    char **dict = read_dictionary(argv[2], &dlen);

    // Open the hash file for reading.
    FILE *h = fopen(argv[1], "r");
    if (!h)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }
    
    // For each hash, try every entry in the dictionary.
    char hash[HASH_LEN];
    int count = 1;
    while (fgets(hash, HASH_LEN, h) != NULL)        // Loop through hashfile
    {
        hash[strlen(hash) + 1] = '\0';              // Trim off newline
        
        for (int j = 0; j < dlen; j++)              // Loop through dictionary
        {
            if (tryguess(hash, dict[j]) == 1)       // Find match
            {
                // Print the matching dictionary entry.
                printf("%d Hash: %s = %s\n", count, hash, dict[j]);
                count++;
            }
        }
    }
    
    // Free up memory
    fclose(h);
    free(dict[0]);
    free(dict);
    //free(lines);
    
}
